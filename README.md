# Neko VLC RPC

Simple RPC for VLC for the social platform Discord.

## Setup
Run `npm i` inside the Neko VLC RPC directory to install all required modules (droopy-vlc and discord-rpc)

For this to work, you'll have to setup the HTTP VLC module covered [here](https://wiki.videolan.org/Documentation:Modules/http_intf/#VLC_2.0.0_and_later)

config.json holds all the necessary info. You only have to change the setting: `vlcHTTPpw` to whatever you set it to when you setup the HTTP VLC module.
Enjoy!
